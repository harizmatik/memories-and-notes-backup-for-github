package com.sharipov.memoriesandnotes


import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupActionBarWithNavController
import com.sharipov.memoriesandnotes.data.LocalPreferences
import com.sharipov.memoriesandnotes.databinding.ActivityMainBinding
import com.sharipov.memoriesandnotes.model.Theme
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject lateinit var localPreferences: LocalPreferences


    private val selectedTheme: Theme
        get() = localPreferences.appTheme

    private val navController by lazy { findNavController(R.id.nav_host_fragment) }
    private val appBarConfiguration by lazy { AppBarConfiguration.Builder(navController.graph).build() }
    val login: Boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        localPreferences.entriesCount++

        delegate.localNightMode = when(selectedTheme) {
            Theme.DEFAULT_THEME -> AppCompatDelegate.MODE_NIGHT_NO
            Theme.SYSTEM_THEME -> AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
            Theme.DARK_THEME -> AppCompatDelegate.MODE_NIGHT_YES
        }


        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        setSupportActionBar(binding.toolbar)
        setupActionBarWithNavController(navController, appBarConfiguration)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return item.onNavDestinationSelected(navController)
    }

    override fun onBackPressed() {
        moveTaskToBack(true)
    }
}
