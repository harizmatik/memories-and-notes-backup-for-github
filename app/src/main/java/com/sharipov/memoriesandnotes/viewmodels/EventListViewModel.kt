package com.sharipov.memoriesandnotes.viewmodels

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.sharipov.memoriesandnotes.adapters.ItemTouchHelperAdapter
import com.sharipov.memoriesandnotes.data.EventRepository
import com.sharipov.memoriesandnotes.model.Event
import com.sharipov.memoriesandnotes.model.EventDisplaySettings
import kotlinx.coroutines.launch

class EventListViewModel @ViewModelInject constructor(
    private val repository: EventRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel(), ItemTouchHelperAdapter {

    val deletedEvent = MutableLiveData<Event?>()

    val events = repository.getEvents() as MutableLiveData

    fun saveSort() =
        events.value?.map { it.displaySettings }?.let {
            viewModelScope.launch {
                repository.editEventsDisplaySettings(it)
            }
        }

    fun sortBy(sortType: SortType) {
        val sortedList = sortList(sortType)
        val newDSList = events.value!!.map { it.displaySettings }.toMutableList()
        sortedList.forEachIndexed { index, event ->
            val ds = newDSList.find { it.eventId == event.base.id }
            if (ds == null) {
                newDSList.add(EventDisplaySettings(event.base.id, orderId = index+1L))
            } else {
                ds.orderId = index+1L
            }
        }
        viewModelScope.launch { repository.editEventsDisplaySettings(newDSList) }
    }

    private fun sortList(sortType: SortType) = when(sortType) {
        SortType.BY_TITLE_AZ -> events.value!!.sortedBy { it.base.title }
        SortType.BY_TITLE_ZA -> events.value!!.sortedByDescending { it.base.title }
        SortType.BY_DATE_ASC -> events.value!!.sortedBy { it.base.date }
        SortType.BY_DATE_DESC -> events.value!!.sortedByDescending { it.base.date }
        SortType.BY_ADD_DATE_ASC -> events.value!!.sortedBy { it.base.additionOrder }
        SortType.BY_ADD_DATE_DESC -> events.value!!.sortedByDescending { it.base.additionOrder }
    }

    override fun onItemMove(from: Int, to: Int): Boolean {
        if (from < to) {
            for (i in from until to) {
                swapEvents(i, i+1)
            }
        } else {
            for (i in from downTo to + 1) {
                swapEvents(i, i-1)
            }
        }
        return true
    }

    private fun swapEvents(i: Int, i1: Int) {
        val orderFrom = events.value!![i].displaySettings.orderId
        val orderTo = events.value!![i1].displaySettings.orderId
        events.value!![i].displaySettings.orderId = orderTo
        events.value!![i1].displaySettings.orderId = orderFrom
        events.value = events.value!!.sortedBy { it.displaySettings.orderId  }
    }

    override fun onItemDismiss(event: Event) {
        deletedEvent.postValue(event)
        viewModelScope.launch {
            repository.deleteEvent(event.base)
        }
    }

    fun restoreLastDeletedEvent() {
        if (deletedEvent.value != null) {
            viewModelScope.launch {
                repository.addEvent(deletedEvent.value!!.base, deletedEvent.value!!.displaySettings)
                repository.insertEventDisplaySettings(deletedEvent.value!!.displaySettings)
                deletedEvent.postValue(null)
            }
        }
    }

    companion object {

        enum class SortType(val value: Int) {
            BY_TITLE_AZ(0),
            BY_TITLE_ZA(1),
            BY_DATE_ASC(2),
            BY_DATE_DESC(3),
            BY_ADD_DATE_ASC(4),
            BY_ADD_DATE_DESC(5);

            companion object {
                private val map = values().associateBy(SortType::value)
                fun fromInt(type: Int) = map[type]
            }
        }
    }
}