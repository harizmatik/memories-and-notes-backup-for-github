package com.sharipov.memoriesandnotes.viewmodels

import android.net.Uri
import androidx.databinding.ObservableField
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.sharipov.memoriesandnotes.data.BackupRepository
import com.sharipov.memoriesandnotes.data.EventRepository
import com.sharipov.memoriesandnotes.model.EventBase
import kotlinx.coroutines.launch

class DownloadFromFileViewModel @ViewModelInject constructor(
    private val repository: EventRepository,
    private val backupRepository: BackupRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
)
    : ViewModel() {

    val insertionMethod = ObservableField(InsertionMethod.REPLACE)
    val downloadStatus = MutableLiveData(DownloadStatus.FILE_NOT_SELECTED)

    val backupFilename = BackupRepository.BACKUP_FILENAME_FULL
    val backupFileUri: ObservableField<Uri?> = ObservableField(Uri.EMPTY)

    enum class InsertionMethod {
        REPLACE,
        IGNORE,
        SCRATCH
    }

    enum class DownloadStatus {
        FILE_NOT_SELECTED,
        NOT_STARTED,
        SUCCESS,
        FAIL,
        IN_PROCESS
    }

    fun applyEventsFromBackup() {
        downloadStatus.postValue(DownloadStatus.IN_PROCESS)
        viewModelScope.launch {
            val events = backupRepository.readEventsFromBackupFile(backupFileUri.get().toString())
            if (events == null) {
                downloadStatus.postValue(DownloadStatus.FAIL)
            } else {
                insertEvents(events)
                downloadStatus.postValue(DownloadStatus.SUCCESS)
            }
        }
    }

    fun setDownloadStatus(method: InsertionMethod) {
        insertionMethod.set(method)
    }

    fun selectedCorrectBackupFile(uri: Uri) {
        downloadStatus.postValue(DownloadStatus.NOT_STARTED)
        backupFileUri.set(uri)
    }

    private suspend fun insertEvents(events: List<EventBase>) {
        when (insertionMethod.get()) {
            InsertionMethod.REPLACE -> {
                repository.addEventsConflictReplace(events)
            }
            InsertionMethod.IGNORE -> {
                repository.addEventsConflictIgnore(events)
            }
            InsertionMethod.SCRATCH -> {
                repository.deleteAllEvents()
                repository.addEventsConflictIgnore(events)
            }
        }
    }
}