package com.sharipov.memoriesandnotes.viewmodels

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.sharipov.memoriesandnotes.data.EventRepository

class SettingsViewModel @ViewModelInject constructor(
    repository: EventRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    val hasEvents = Transformations.map(repository.getAllEvents()) {
        it.isNotEmpty()
    }
}