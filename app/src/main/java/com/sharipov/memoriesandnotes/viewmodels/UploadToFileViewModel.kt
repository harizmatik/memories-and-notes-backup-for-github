package com.sharipov.memoriesandnotes.viewmodels

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.sharipov.memoriesandnotes.data.BackupRepository
import com.sharipov.memoriesandnotes.data.EventRepository
import kotlinx.coroutines.launch
import java.lang.Exception

class UploadToFileViewModel @ViewModelInject constructor(
    repository: EventRepository,
    private val backupRepository: BackupRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    val events = repository.getAllEvents()
    val uploadStatus = MutableLiveData(UploadStatus.NOT_STARTED)
    lateinit var backupFilename: String
        private set

    enum class UploadStatus {
        SUCCESS,
        FAIL,
        IN_PROCESS,
        NOT_STARTED
    }

    fun uploadEventsToFile() {
        uploadStatus.postValue(UploadStatus.IN_PROCESS)
        viewModelScope.launch {
            try {
                backupFilename = backupRepository.writeEventsToBackupFile(events.value!!)
                uploadStatus.postValue(UploadStatus.SUCCESS)
            } catch (e: Exception) {
                uploadStatus.postValue(UploadStatus.FAIL)
            }
        }
    }
}