package com.sharipov.memoriesandnotes.viewmodels

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.sharipov.memoriesandnotes.data.EventRepository
import kotlinx.coroutines.launch
import java.util.*

class EditEventViewModel @ViewModelInject constructor(
    private val repository: EventRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val eventId: Long = savedStateHandle.get<Long>("eventId")!!

    val event = repository.getEvent(eventId)

    fun isRequiredFieldsFilled() = !event.value?.base?.title.isNullOrEmpty()

    fun changeEventDate(year: Int, month: Int, dayOfMonth: Int) {
        val newDate = Calendar.getInstance().apply { set(year, month, dayOfMonth) }.time
        val newBase = event.value!!.base.copy(date = newDate)
        event.value = (event.value?.copy(base = newBase))
    }

    fun saveEditedEvent() {
        viewModelScope.launch {
            event.value?.let { repository.editEvent(it.base) }
        }
    }
}