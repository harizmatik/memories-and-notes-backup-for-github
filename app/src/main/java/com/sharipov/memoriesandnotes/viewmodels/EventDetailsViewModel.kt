package com.sharipov.memoriesandnotes.viewmodels

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.sharipov.memoriesandnotes.data.EventRepository
import com.sharipov.memoriesandnotes.model.EventDisplaySettings.CountingMethod
import com.sharipov.memoriesandnotes.model.EventDisplaySettings.DefinedColors
import com.sharipov.memoriesandnotes.model.*
import kotlinx.coroutines.launch

class EventDetailsViewModel @ViewModelInject constructor(
    private val repository: EventRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val eventId: Long = savedStateHandle.get<Long>("eventId")!!

    val event = repository.getEvent(eventId)

    val timestampCounters = Transformations.map(event) {
        if (it != null) {
            val countingMethods = it.displaySettings.countingMethods
            TimestampCounters.countForEvent(it.base, countingMethods)
        } else {
            TimestampCounters()
        }
    } as MutableLiveData

    val customColor = MutableLiveData(DefinedColors.CUSTOM.int)

    fun deleteEvent() =
        viewModelScope.launch {
            event.value?.let { repository.deleteEvent(it.base) }
        }

    fun saveEventDisplaySettings() =
        viewModelScope.launch {
            event.value?.let {
                repository.insertEventDisplaySettings(it.displaySettings)
            }
        }

    fun updateCountingMethod(method: CountingMethod) {
        val safeEventDS = event.value!!.displaySettings
        val mutableSet = safeEventDS.countingMethods.toMutableSet()
        // Метод уже добавлен
        if (mutableSet.contains(method)) {
            mutableSet.remove(method)
        } else {
            mutableSet.add(method)
        }
        val newEventDS = safeEventDS.copy(countingMethods = mutableSet)
        event.value = event.value?.copy(displaySettings = newEventDS)
    }

    fun updateColor(color: DefinedColors) {
        val newColor =
            if (color != DefinedColors.CUSTOM) color.int
            else customColor.value!!
        val newEventDS = event.value!!.displaySettings.copy(color = newColor)
        event.value = event.value?.copy(displaySettings = newEventDS)
    }

    fun isCountingMethodSelected(method: CountingMethod) =
        event.value?.displaySettings?.countingMethods?.contains(method) == true ||
            timestampCounters.value?.isSelected(method) == true
}