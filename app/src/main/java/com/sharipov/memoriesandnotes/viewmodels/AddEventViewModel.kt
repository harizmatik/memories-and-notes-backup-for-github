package com.sharipov.memoriesandnotes.viewmodels

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.sharipov.memoriesandnotes.model.EventBase
import com.sharipov.memoriesandnotes.data.EventRepository
import kotlinx.coroutines.launch
import java.util.*

class AddEventViewModel @ViewModelInject constructor(
    private val repository: EventRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    val event: MutableLiveData<EventBase> = MutableLiveData(
        EventBase(title = "", description = ""))

    fun isRequiredFieldsFilled() = !event.value?.title.isNullOrEmpty()

    fun changeEventDate(year: Int, month: Int, dayOfMonth: Int) {
        val newDate = Calendar.getInstance().apply { set(year, month, dayOfMonth) }.time
        event.value = (event.value?.copy(date = newDate))
    }

    fun addEvent() {
        viewModelScope.launch {
            event.value?.let {
                repository.addEvent(it)
            }
        }
    }
}