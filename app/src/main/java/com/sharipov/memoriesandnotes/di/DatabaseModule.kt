package com.sharipov.memoriesandnotes.di

import android.content.Context
import com.sharipov.memoriesandnotes.data.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
object DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext appContext: Context) =
        AppDatabase.buildDatabase(appContext)

    @Provides
    fun provideEventBaseDao(database: AppDatabase) =
        database.eventBaseDao()

    @Provides
    fun provideEventDao(database: AppDatabase) =
        database.eventWithDisplaySettingsDao()

    @Provides
    fun provideDisplaySettingsDao(database: AppDatabase) =
        database.displaySettingsDao()
}