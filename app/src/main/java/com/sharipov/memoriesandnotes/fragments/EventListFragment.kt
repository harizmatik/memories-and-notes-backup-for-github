package com.sharipov.memoriesandnotes.fragments

import android.app.AlertDialog
import android.appwidget.AppWidgetManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.onNavDestinationSelected
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar

import com.sharipov.memoriesandnotes.R
import com.sharipov.memoriesandnotes.adapters.EventListAdapter
import com.sharipov.memoriesandnotes.adapters.SimpleItemTouchHelperCallback
import com.sharipov.memoriesandnotes.data.AppPreferences
import com.sharipov.memoriesandnotes.data.LocalPreferences
import com.sharipov.memoriesandnotes.databinding.FragmentEventListBinding
import com.sharipov.memoriesandnotes.model.Event
import com.sharipov.memoriesandnotes.utilities.DateFormatter
import com.sharipov.memoriesandnotes.viewmodels.EventListViewModel
import com.sharipov.memoriesandnotes.viewmodels.EventListViewModel.Companion.SortType
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class EventListFragment : Fragment() {

    private val viewModel: EventListViewModel by viewModels()

    private val navController by lazy { findNavController()}
    @Inject
    lateinit var localPreferences: LocalPreferences
    @Inject
    lateinit var appPreferences: AppPreferences
    private lateinit var binding: FragmentEventListBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = FragmentEventListBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            recyclerView.adapter = eventListAdapter
            val itemTouchHelper =
                ItemTouchHelper(SimpleItemTouchHelperCallback(viewModel))
            itemTouchHelper.attachToRecyclerView(recyclerView)
            // листенер для скрытия fab если список прокручен вниз
            recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    if (localPreferences.useRoundIcon) {
                        if (dy > 0) fabAdd.hide() else fabAdd.show()
                    }
                    super.onScrolled(recyclerView, dx, dy)
                }
            })
            fabAdd.setOnClickListener { actionAddEvent() }
            subscribeUi(eventListAdapter)
        }

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        val dateFormatStyle = appPreferences.dateFormatStyle
        binding.todayTextView.text = getString(R.string.today, DateFormatter(dateFormatStyle).format(Date()))
        binding.fabAdd.visibility =
            if (localPreferences.useRoundIcon) View.VISIBLE else View.GONE
    }

    override fun onPause() {
        super.onPause()
        viewModel.saveSort()
    }

    private fun actionAddEvent() = navController.navigate(R.id.action_add_event)

    private val eventClickListener = View.OnClickListener {
        // получаем событие, оно был установлен ранее в onBindViewHolder адаптера
        val event = it.tag as Event
        val direction = EventListFragmentDirections.actionEventDetails(event.base.id)
        navController.navigate(direction)
    }

    private val eventListAdapter = EventListAdapter().apply {
        onItemClickListener = eventClickListener
    }


    private fun subscribeUi(adapter: EventListAdapter) {
        viewModel.events.observe(viewLifecycleOwner) { list ->
            binding.hasEvents = !list.isNullOrEmpty()
            localPreferences.userHasEvents = !list.isNullOrEmpty()
            if (list != null) {
                adapter.submitList(list)
            }
        }

        viewModel.deletedEvent.observe(viewLifecycleOwner) {
            if (it != null) {
                showRestoreSnackbar(it)
            } else {
                eventListAdapter.submitList(viewModel.events.value)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_event_list_fragment, menu)
        menu.findItem(R.id.action_add_event).isVisible = !localPreferences.useRoundIcon
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_sort -> {
            showSortDialog()
            true
        }
        R.id.action_add_event -> {
            actionAddEvent()
            true
        }
        else -> {
            item.onNavDestinationSelected(navController)
        }
    }

    private fun showRestoreSnackbar(event: Event) {
        // Дать возможность восстановить удаленное событие
        Snackbar.make(binding.root, R.string.event_deleted_warning, Snackbar.LENGTH_SHORT)
            // Удалять виджет, только после того, как пропадет возможность восстановить
            .addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
                override fun onDismissed(transientBottomBar: Snackbar?, id: Int) {
                    super.onDismissed(transientBottomBar, id)
                    viewModel.deletedEvent.postValue(null)
                }
            })
            .setAction(R.string.event_restore) {
                viewModel.restoreLastDeletedEvent()
            }
            .show()
    }

    private fun showSortDialog() {
        AlertDialog.Builder(requireContext()).apply {
            setTitle(getString(R.string.action_sort))
            setItems(R.array.sort_options) { _, which ->
                val sortType = SortType.fromInt(which)
                sortType?.let { viewModel.sortBy(sortType) }
            }
            setNegativeButton(R.string.cancel, null)
            create()
        }.show()
    }
}
