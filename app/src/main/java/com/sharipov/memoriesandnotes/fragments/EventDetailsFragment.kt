package com.sharipov.memoriesandnotes.fragments

import android.app.AlertDialog
import android.appwidget.AppWidgetManager
import android.content.DialogInterface
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.flask.colorpicker.ColorPickerView
import com.flask.colorpicker.builder.ColorPickerDialogBuilder
import com.sharipov.memoriesandnotes.R
import com.sharipov.memoriesandnotes.databinding.FragmentEventDetailsBinding
import com.sharipov.memoriesandnotes.model.EventBase
import com.sharipov.memoriesandnotes.model.EventDisplaySettings.DefinedColors.*
import com.sharipov.memoriesandnotes.model.EventDisplaySettings.DefinedColors
import com.sharipov.memoriesandnotes.model.EventDisplaySettings.CountingMethod
import com.sharipov.memoriesandnotes.viewmodels.EventDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class EventDetailsFragment : Fragment() {

    private val args: EventDetailsFragmentArgs by navArgs()
    private val navController by lazy { findNavController() }
    private val viewModel: EventDetailsViewModel by viewModels()
    private lateinit var binding: FragmentEventDetailsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentEventDetailsBinding.inflate(inflater, container, false).apply {
            vm = viewModel
            lifecycleOwner = viewLifecycleOwner
            buttonManualColorPicker.setOnClickListener { createColorPickerDialog() }
            subscribeUI()
            daysCheckBox.isChecked =
                viewModel.isCountingMethodSelected(CountingMethod.DAYS)
            weeksCheckBox.isChecked =
                viewModel.isCountingMethodSelected(CountingMethod.WEEKS)
            monthsCheckBox.isChecked =
                viewModel.isCountingMethodSelected(CountingMethod.MONTHS)
            yearsCheckBox.isChecked =
                viewModel.isCountingMethodSelected(CountingMethod.YEARS)
        }

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onPause() {
        super.onPause()
        viewModel.saveEventDisplaySettings()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) =
        inflater.inflate(R.menu.menu_event_details_fragment, menu)

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_edit -> {
                val direction = EventDetailsFragmentDirections.actionEditEventFromDetails(args.eventId)
                navController.navigate(direction)
            }
            R.id.action_delete -> {
                confirmDeletionDialog.show()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun createColorPickerDialog() {
        ColorPickerDialogBuilder
            .with(context)
            .setTitle(R.string.choose_color)
            .initialColor(CUSTOM.int)
            .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
            .density(12)
            .lightnessSliderOnly()
            .setPositiveButton(R.string.action_save) { _, selectedColor, _ ->
                viewModel.customColor.value = selectedColor
                viewModel.updateColor(CUSTOM)
            }
            .setNegativeButton(R.string.cancel, null)
            .build()
            .show()
    }

    private fun subscribeUI() {
        // Если пользователь не предоставил описание, то показываем описание по умолчанию
        viewModel.event.observe(viewLifecycleOwner) {
            if (it != null) {
                it.base.description = it.base.description.ifEmpty { getString(R.string.default_description) }
                selectColorButton(it.displaySettings.color)
            }
        }
        viewModel.customColor.observe(viewLifecycleOwner) {}
        viewModel.timestampCounters.observe(viewLifecycleOwner) {}
    }

    private fun selectColorButton(color: Int) {
        binding.apply {
            buttonOrangeColor.setImageDrawable(null)
            buttonRedColor.setImageDrawable(null)
            buttonPurpleColor.setImageDrawable(null)
            buttonBlueColor.setImageDrawable(null)
            buttonGreenColor.setImageDrawable(null)
            buttonYellowColor.setImageDrawable(null)
            buttonCustomColor.setImageDrawable(null)

            when (DefinedColors.convert(color)) {
                ORANGE -> binding.buttonOrangeColor
                RED -> binding.buttonRedColor
                PURPLE -> binding.buttonPurpleColor
                BLUE -> binding.buttonBlueColor
                GREEN -> binding.buttonGreenColor
                YELLOW -> binding.buttonYellowColor
                CUSTOM -> binding.buttonCustomColor
            }.setImageResource(R.drawable.ic_check_round)
        }
    }


    private val confirmDeletionDialog by lazy {
        AlertDialog.Builder(requireContext()).apply {
            setTitle(R.string.confirm_deletion_dialog_title)
            setPositiveButton(R.string.confirm_deletion_dialog_positive_button) { _: DialogInterface, _: Int ->
                viewModel.deleteEvent()
                navController.popBackStack()
            }
            setNegativeButton(R.string.confirm_deletion_dialog_negative_button, null)
            setMessage(R.string.confirm_deletion_dialog_message)
        }.create()
    }
}
