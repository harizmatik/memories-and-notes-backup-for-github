package com.sharipov.memoriesandnotes.fragments

import android.R.attr.key
import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.preference.*
import com.anjlab.android.iab.v3.BillingProcessor
import com.sharipov.memoriesandnotes.R
import com.sharipov.memoriesandnotes.data.AppPreferences
import com.sharipov.memoriesandnotes.login.SetPassword
import com.sharipov.memoriesandnotes.model.DateFormatStyle
import com.sharipov.memoriesandnotes.utilities.DateFormatter
import com.sharipov.memoriesandnotes.viewmodels.SettingsViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject


@AndroidEntryPoint
class SettingsFragment : PreferenceFragmentCompat() {

    private var billingProcessor: BillingProcessor? = null
    private val navController by lazy { findNavController()}
    @Inject
    lateinit var appPreferences: AppPreferences

    private val viewModel: SettingsViewModel by viewModels()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)

        val contactUsPref: Preference = findPreference(CONTACT_US_KEY)!!
        val uploadToFilePref: Preference = findPreference(UPLOAD_TO_FILE_KEY)!!
        val downloadFromFilePref: Preference = findPreference(DOWNLOAD_FROM_FILE_KEY)!!
        val darkThemePref: SwitchPreference = findPreference(DARK_THEME)!!
        val useSystemThemePref: CheckBoxPreference = findPreference(USE_SYSTEM_THEME)!!
        val dateFormatStylePref: ListPreference = findPreference(DATE_FORMAT_STYLE)!!

        val setPassword: SwitchPreference = findPreference(SET_PASSWORD_BUTTON)!!
        val setPasswordToLogin: Preference = findPreference(SET_PASSWORD_TO_LOGIN)!!

        contactUsPref.setOnPreferenceClickListener { actionContactUs(); true }
        uploadToFilePref.setOnPreferenceClickListener { actionUploadToFile(); true }
        downloadFromFilePref.setOnPreferenceClickListener { actionDownloadFromFile(); true }
        darkThemePref.setOnPreferenceClickListener { actionChangeTheme(); true }
        useSystemThemePref.setOnPreferenceClickListener { actionChangeTheme(); true }

        dateFormatStylePref.setOnPreferenceChangeListener { preference, newValue ->
            val dateFormatStyle = DateFormatStyle.toDateFormatStyle(newValue as String)
            appPreferences.dateFormatStyle = dateFormatStyle
            preference.summary = DateFormatter(dateFormatStyle).format(Date(0))
            true
        }

        setPassword.setOnPreferenceChangeListener { _, newValue ->
            appPreferences.usePassword = newValue as Boolean
            securityVisible()
            true
        }

        setPasswordToLogin.setOnPreferenceClickListener { actionSetPasswordToLogin(); true }

        securityVisible()



        val dateFormatStyle = DateFormatStyle.toDateFormatStyle(dateFormatStylePref.value as String)
        dateFormatStylePref.summary = DateFormatter(dateFormatStyle).format(Date(0))
    }

    override fun onResume() {
        super.onResume()
        securityVisible()
    }

    @SuppressLint("QueryPermissionsNeeded")
    private fun actionContactUs() {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(SUPPORT_EMAIL_ADDRESS))
        val resInfo = requireActivity().packageManager.queryIntentActivities(intent, 0)
        resInfo.find { it?.activityInfo?.packageName == GMAIL_APP_PACKAGE_ID }
        resInfo.let { intent.setPackage(GMAIL_APP_PACKAGE_ID) }
        requireActivity().startActivity(intent)
    }

    private fun actionUploadToFile() {
        viewModel.hasEvents.observe(viewLifecycleOwner) {
            if (it == true) {
                 navController.navigate(R.id.action_upload_to_file)
            } else if (it == false) {
                Toast.makeText(
                    requireContext(),
                    getString(R.string.upload_to_file_error_empty_list),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun actionDownloadFromFile() {
        navController.navigate(R.id.action_download_from_file)
    }

    private fun actionChangeTheme() {
        requireActivity().recreate()
    }

    private fun actionSetPasswordToLogin(){
        requireActivity().startActivity(Intent(requireActivity(), SetPassword::class.java))
    }

    private fun securityVisible(){
        val setPassword: SwitchPreference = findPreference(SET_PASSWORD_BUTTON)!!
        val setPasswordToLogin: Preference = findPreference(SET_PASSWORD_TO_LOGIN)!!
        if (appPreferences.usePassword) {
            setPasswordToLogin.isVisible = true
            if(appPreferences.password == "PasswordNotExist"){
                requireActivity().startActivity(Intent(requireActivity(), SetPassword::class.java))
            }
        }
        else{
            setPasswordToLogin.isVisible = false
            if(appPreferences.password != "PasswordNotExist"){
                appPreferences.password = null
            }
        }
        setPassword.isChecked = appPreferences.usePassword
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (billingProcessor?.handleActivityResult(requestCode, resultCode, data) != true) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        billingProcessor?.release()
    }

    companion object {
        private const val CONTACT_US_KEY = "contact_us"
        private const val UPLOAD_TO_FILE_KEY = "upload_to_file"
        private const val DOWNLOAD_FROM_FILE_KEY = "download_from_file"
        private const val DARK_THEME = "dark_theme"
        private const val USE_SYSTEM_THEME = "use_system_theme"
        private const val DATE_FORMAT_STYLE = "date_format_style"

        private const val SET_PASSWORD_BUTTON = "set_password_button"
        const val SET_PASSWORD_TO_LOGIN = "set_password_to_login"

        private const val GMAIL_APP_PACKAGE_ID = "com.google.android.gm"
        private const val SUPPORT_EMAIL_ADDRESS = "mailto:akbar.sharipov.apps@gmail.com"
    }
}
