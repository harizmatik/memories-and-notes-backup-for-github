package com.sharipov.memoriesandnotes.fragments

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController

import com.sharipov.memoriesandnotes.R
import com.sharipov.memoriesandnotes.data.LocalPreferences
import com.sharipov.memoriesandnotes.databinding.FragmentAddEventBinding
import com.sharipov.memoriesandnotes.viewmodels.AddEventViewModel
import dagger.hilt.android.AndroidEntryPoint

import java.util.Calendar
import javax.inject.Inject

@AndroidEntryPoint
class AddEventFragment : Fragment() {

    private val viewModel: AddEventViewModel by viewModels()

    private val navController by lazy { findNavController()}
    @Inject
    lateinit var localPreferences: LocalPreferences
    private lateinit var binding: FragmentAddEventBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentAddEventBinding.inflate(inflater, container, false).apply {
            viewModel = this@AddEventFragment.viewModel
            lifecycleOwner = viewLifecycleOwner
            fabSave.setOnClickListener { actionSave() }
            setupDateEditText(this)
        }

        setHasOptionsMenu(true)

        return binding.root
    }

    // для того чтобы не открывалось больше одного диалога выбора даты
    private var datePickerOnScreen: Boolean = false

    private fun setupDateEditText(binding: FragmentAddEventBinding) {
        binding.date.apply {
            onFocusChangeListener = dateFocusChangeListener
            setOnClickListener(dateOnClickListener)
        }
        binding.pickDateButton.setOnClickListener(dateOnClickListener)
    }

    private fun createDatePickerDialog() {
        datePickerOnScreen = true
        val calendar = Calendar.getInstance()
        DatePickerDialog(
            requireActivity(),
            datePickerSetListener,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        ).apply {
            setOnDismissListener { datePickerOnScreen = false }
        }.show()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_add_event_fragment, menu)
        menu.findItem(R.id.action_save).isVisible = !localPreferences.useRoundIcon
    }

    override fun onResume() {
        super.onResume()
        binding.fabSave.visibility = if (localPreferences.useRoundIcon) View.VISIBLE else View.GONE
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_save -> {
                actionSave()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private val datePickerSetListener = DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
        viewModel.changeEventDate(year, month, dayOfMonth)
    }

    private val dateFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
        if (hasFocus && !datePickerOnScreen) {
            createDatePickerDialog()
        }
    }

    private val dateOnClickListener = View.OnClickListener {
        if (!datePickerOnScreen) {
            createDatePickerDialog()
        }
    }

    private fun actionSave() {
        if (viewModel.isRequiredFieldsFilled()) {
            viewModel.addEvent()
            navController.popBackStack()
        } else {
            warningEmptyFields()
        }
    }

    private fun warningEmptyFields() {
        binding.title.requestFocus()
        Toast.makeText(requireContext(), R.string.warning_empty_fields, Toast.LENGTH_LONG).show()
    }
}
