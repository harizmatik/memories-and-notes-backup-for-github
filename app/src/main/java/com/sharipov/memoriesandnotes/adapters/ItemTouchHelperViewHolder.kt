package com.sharipov.memoriesandnotes.adapters

interface ItemTouchHelperViewHolder {
    fun onItemSelected()
    fun onItemClear()
}