@file:Suppress("DEPRECATION")

package com.sharipov.memoriesandnotes.adapters

import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.databinding.BindingAdapter
import com.sharipov.memoriesandnotes.R
import com.sharipov.memoriesandnotes.data.AppPreferences
import com.sharipov.memoriesandnotes.model.*
import com.sharipov.memoriesandnotes.utilities.DateFormatter
import com.sharipov.memoriesandnotes.viewmodels.DownloadFromFileViewModel.DownloadStatus
import com.sharipov.memoriesandnotes.viewmodels.UploadToFileViewModel.UploadStatus
import java.util.*
import kotlin.math.absoluteValue


@BindingAdapter("app:isGone")
fun bindIsGone(view: View, isGone: Boolean) {
    view.visibility = if (isGone) {
        View.GONE
    } else {
        View.VISIBLE
    }
}

@BindingAdapter("app:timestampsCounters")
fun bindTimestampsCounters(textView: TextView, timestampCounters: TimestampCounters?) {
    var string = ""
    timestampCounters?.let { tc ->
        tc.years?.let { string += "$it/" }
        tc.months?.let { string += "$it/" }
        tc.weeks?.let { string += "$it/" }
        tc.days?.let { string += "$it/" }
    }
    textView.text = if (string.isEmpty()) {
        "-"
    } else {
        string.dropLast(1)
    }
}

@BindingAdapter("app:eventTimestampsActivated")
fun bindTimestampsActivated(textView: TextView, timestampCounters: TimestampCounters?) {
    var string = ""
    val res = textView.resources
    timestampCounters?.let { tc ->
        tc.years?.let { string += "${res.getQuantityString(R.plurals.short_years, it)}/" }
        tc.months?.let { string += "${res.getQuantityString(R.plurals.short_months, it)}/" }
        tc.weeks?.let { string += "${res.getQuantityString(R.plurals.short_weeks, it)}/" }
        tc.days?.let { string += "${res.getQuantityString(R.plurals.short_days, it)}/" }
    }
    textView.text = if (string.isEmpty()) {
        "-"
    } else {
        string.dropLast(1)
    }
}

@BindingAdapter("app:eventTimeStatus")
fun bindEventTimeStatus(textView: TextView, event: EventBase?) {
    val string = event?.let {
        textView.resources.getQuantityString(it.getEventTimeStatusResId(), it.countDaysToToday().absoluteValue)
    }
    textView.text = string ?: ""
}

@BindingAdapter("android:text")
fun bindTextDate(textView: TextView, date: Date?) {
    if (date != null) {
        val dateFormatStyle = AppPreferences(textView.context).dateFormatStyle
        textView.text = DateFormatter(dateFormatStyle).format(date)
    }
}

@BindingAdapter("android:textColor")
fun bindUploadStatusTextColor(textView: TextView, status: UploadStatus?) {
    val color = when(status) {
        UploadStatus.SUCCESS -> textView.resources.getColor(R.color.colorSuccess)
        UploadStatus.FAIL -> textView.resources.getColor(R.color.colorFail)
        else -> textView.resources.getColor(R.color.colorAccent)
    }
    textView.setTextColor(color)
}

@BindingAdapter("android:textColor")
fun bindDownloadStatusTextColor(textView: TextView, status: DownloadStatus?) {
    val color = when(status) {
        DownloadStatus.SUCCESS -> textView.resources.getColor(R.color.colorSuccess)
        DownloadStatus.FAIL -> textView.resources.getColor(R.color.colorFail)
        else -> textView.resources.getColor(R.color.colorAccent)
    }
    textView.setTextColor(color)
}

@BindingAdapter("android:progress")
fun bindUploadStatusProgress(progressBar: ProgressBar, status: UploadStatus?) {
    progressBar.progress = when(status) {
        UploadStatus.FAIL, UploadStatus.SUCCESS -> 2
        UploadStatus.IN_PROCESS -> 1
        else -> 0
    }
}

@BindingAdapter("android:progress")
fun bindDownloadStatusProgress(progressBar: ProgressBar, status: DownloadStatus?) {
    progressBar.progress = when(status) {
        DownloadStatus.FAIL, DownloadStatus.SUCCESS -> 2
        DownloadStatus.IN_PROCESS -> 1
        else -> 0
    }
}

@BindingAdapter("android:text")
fun bindDownloadStatusText(textView: TextView, status: DownloadStatus?) {
    textView.text = when(status) {
        DownloadStatus.SUCCESS -> textView.resources.getString(R.string.status_success)
        DownloadStatus.FAIL -> textView.resources.getString(R.string.status_fail)
        DownloadStatus.IN_PROCESS -> textView.resources.getString(R.string.status_in_process)
        else -> ""
    }
}

@BindingAdapter("android:text")
fun bindUploadStatusText(textView: TextView, status: UploadStatus?) {
    textView.text = when(status) {
        UploadStatus.SUCCESS -> textView.resources.getString(R.string.status_success)
        UploadStatus.FAIL -> textView.resources.getString(R.string.status_fail)
        UploadStatus.IN_PROCESS -> textView.resources.getString(R.string.status_in_process)
        else -> ""
    }
}

@BindingAdapter("app:cardBackgroundColor")
fun bindDefinedColorsCardView(cardView: CardView, color: EventDisplaySettings.DefinedColors) {
    cardView.setCardBackgroundColor(color.int)
}