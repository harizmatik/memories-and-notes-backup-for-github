package com.sharipov.memoriesandnotes.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sharipov.memoriesandnotes.R
import com.sharipov.memoriesandnotes.databinding.EventListItemBinding
import com.sharipov.memoriesandnotes.model.Event
import com.sharipov.memoriesandnotes.model.TimestampCounters

class EventListAdapter :
    ListAdapter<Event,
        EventListAdapter.ViewHolder>(EventDiffCallback()) {

    var onItemClickListener: View.OnClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(EventListItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val event = getItem(position)
        holder.apply {
            bind(event, onItemClickListener)
            // устанавливаем тег - само событие, чтобы потом обработать нажатия
            itemView.tag = event
        }
    }

    class ViewHolder(
        private val binding: EventListItemBinding
    ) : RecyclerView.ViewHolder(binding.root), ItemTouchHelperViewHolder {
        fun bind(event: Event, clickListener: View.OnClickListener?) {
            binding.apply {
                this.event = event
                this.timestampCounters =
                    TimestampCounters.countForEvent(event.base, event.displaySettings.countingMethods)
                this.clickListener = clickListener
                executePendingBindings()
            }
        }

        override fun onItemClear() {
            val backgroundColor = ContextCompat.getColor(
                itemView.context, R.color.colorCardViewBackground)
            binding.cardView.setCardBackgroundColor(backgroundColor)
        }

        override fun onItemSelected() {
            val selectedBackgroundColor = ContextCompat.getColor(
                itemView.context, R.color.colorSelectedCardViewBackground)
            binding.cardView.setCardBackgroundColor(selectedBackgroundColor)
        }
    }
}

private class EventDiffCallback : DiffUtil.ItemCallback<Event>() {
    override fun areItemsTheSame(oldItem: Event, newItem: Event): Boolean {
        return oldItem.base.id == newItem.base.id
    }

    override fun areContentsTheSame(oldItem: Event, newItem: Event): Boolean {
        return oldItem == newItem
    }
}