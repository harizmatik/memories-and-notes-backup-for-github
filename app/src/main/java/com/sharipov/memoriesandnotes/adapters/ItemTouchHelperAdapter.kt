package com.sharipov.memoriesandnotes.adapters

import com.sharipov.memoriesandnotes.model.Event

interface ItemTouchHelperAdapter {
    fun onItemMove(from: Int, to: Int): Boolean
    fun onItemDismiss(event: Event)
}