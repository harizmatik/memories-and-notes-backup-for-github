package com.sharipov.memoriesandnotes.data

import android.appwidget.AppWidgetManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.sharipov.memoriesandnotes.model.EventBase
import com.sharipov.memoriesandnotes.model.EventDisplaySettings
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EventRepository @Inject constructor(
    private val eventsDao: EventWithDisplaySettingsDao,
    private val eventBaseDao: EventBaseDao,
    private val displaySettingsDao: EventDisplaySettingsDao
) {

    fun getEvents() =
        Transformations.map(eventsDao.getAllEventsWithDisplaySettings()) { list ->
            list?.map { it.toEvent() }
                ?.sortedBy { it.displaySettings.orderId }
        }

    fun getEvent(id: Long) =
        Transformations.map(eventsDao.getEventWithDisplaySettings(id)) {
            it.toEvent()
        } as MutableLiveData

    fun getAllEvents() = eventBaseDao.getAllEvents()


    suspend fun addEvent(event: EventBase, displaySettings: EventDisplaySettings? = null) {
        withContext(IO) {
            val id = eventBaseDao.insert(event)
            val forSave = displaySettings ?: EventDisplaySettings(id)
            displaySettingsDao.insert(forSave)
        }
    }

    suspend fun addEventsConflictIgnore(events: List<EventBase>) {
        withContext(IO) { eventBaseDao.insertEventsConflictIgnore(events) }
    }

    suspend fun addEventsConflictReplace(events: List<EventBase>) {
        withContext(IO) { eventBaseDao.insert(events) }
    }

    suspend fun deleteEvent(event: EventBase) {
        withContext(IO) {
            eventBaseDao.delete(event)
            displaySettingsDao.deleteEventDisplaySettingsById(event.id)
        }
    }

    suspend fun deleteWidgetForEvent(event: EventBase) {
        withContext(IO) { eventBaseDao.update(event) }
    }

    suspend fun editEvent(event: EventBase) {
        withContext(IO) { eventBaseDao.update(event) }
    }

    suspend fun deleteAllEvents() {
        withContext(IO) {
            eventBaseDao.deleteAllEvents()
            displaySettingsDao.deleteAllEventsDisplaySettings()
        }
    }

    suspend fun insertEventDisplaySettings(eventDisplaySettings: EventDisplaySettings) {
        withContext(IO) {
            displaySettingsDao.insert(eventDisplaySettings)
        }
    }

    suspend fun editEventsDisplaySettings(eventsDisplaySettings: List<EventDisplaySettings>) {
        withContext(IO) {
            displaySettingsDao.update(eventsDisplaySettings)
        }
    }
}
