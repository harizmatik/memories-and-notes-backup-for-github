package com.sharipov.memoriesandnotes.data

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.sharipov.memoriesandnotes.model.EventBase
import com.sharipov.memoriesandnotes.model.EventDeserializer
import com.sharipov.memoriesandnotes.model.EventSerializer
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.lang.Exception
import java.lang.reflect.Type
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BackupRepository @Inject constructor(
    @ApplicationContext appContext: Context
) {

    private val backupPath: String = appContext.filesDir.absolutePath
    private val contentResolver: ContentResolver = appContext.contentResolver

    private val gson by lazy {
        GsonBuilder()
            .registerTypeAdapter(EventBase::class.java, EventSerializer())
            .registerTypeAdapter(EventBase::class.java, EventDeserializer())
            .create()
    }

    suspend fun readEventsFromBackupFile(uri: String): List<EventBase>? = withContext(IO) {
        try {
            val result = ArrayList<EventBase>()

            val descriptor = contentResolver.openFileDescriptor(Uri.parse(uri), "r")!!
            val reader = FileReader(descriptor.fileDescriptor)

            val json = reader.readText()
            val eventListType: Type = object : TypeToken<List<EventBase>>() {}.type
            result.addAll(gson.fromJson<List<EventBase>>(json, eventListType))
            return@withContext result
        } catch (e: Exception) {
            return@withContext null
        }
    }

    suspend fun writeEventsToBackupFile(events: List<EventBase>): String = withContext(IO) {
        val backupFile = createNewBackupFile()

        FileWriter(backupFile).let {
            gson.toJson(events, it)
            it.flush()
            it.close()
        }
        backupFile.setReadOnly()

        return@withContext backupFile.absolutePath
    }

    private fun createNewBackupFile(): File {
        val backupFile = File(backupPath, BACKUP_FILENAME_FULL)
        if (backupFile.exists()) {
            backupFile.delete()
        }
        backupFile.createNewFile()
        return backupFile
    }

    companion object {
        private const val BACKUP_FILENAME = "backup"
        private const val BACKUP_FILENAME_EXT = "txt"
        const val BACKUP_FILENAME_FULL = "$BACKUP_FILENAME.$BACKUP_FILENAME_EXT"
    }
}