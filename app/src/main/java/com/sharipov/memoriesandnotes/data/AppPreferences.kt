package com.sharipov.memoriesandnotes.data

import android.content.Context
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.sharipov.memoriesandnotes.BuildConfig
import com.sharipov.memoriesandnotes.model.DateFormatStyle
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppPreferences @Inject constructor(@ApplicationContext context: Context) {

    private val preferences = context.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE)

    var dateFormatStyle: DateFormatStyle
        get() = DateFormatStyle.toDateFormatStyle(
            preferences.getString(DATE_FORMAT_STYLE, "1") ?: "1")
        set(value) = preferences.edit {
            putString(DATE_FORMAT_STYLE, DateFormatStyle.fromDateFormatStyle(value)) }

    var usePassword: Boolean
        get() = preferences.getBoolean(SET_PASSWORD_BUTTON, false)
        set(value) = preferences.edit {
            putBoolean(SET_PASSWORD_BUTTON, value) }

    var password: String?
        get() = preferences.getString(SET_PASSWORD_TO_LOGIN, "PasswordNotExist")
        set(value) = preferences.edit {
            putString(SET_PASSWORD_TO_LOGIN, value) }


    companion object {
        private const val DATE_FORMAT_STYLE = "date_format_style"
        private const val SET_PASSWORD_BUTTON = "set_password_button"
        private const val SET_PASSWORD_TO_LOGIN = "set_password_to_login"
    }
}