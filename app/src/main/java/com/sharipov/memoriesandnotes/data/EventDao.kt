package com.sharipov.memoriesandnotes.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.sharipov.memoriesandnotes.model.EventWithDisplaySettings

@Dao
interface EventWithDisplaySettingsDao {

    @Transaction
    @Query("SELECT * FROM events")
    fun getAllEventsWithDisplaySettings(): LiveData<List<EventWithDisplaySettings>>

    @Transaction
    @Query("SELECT * FROM events WHERE id = :id")
    fun getEventWithDisplaySettings(id: Long): LiveData<EventWithDisplaySettings>

}
