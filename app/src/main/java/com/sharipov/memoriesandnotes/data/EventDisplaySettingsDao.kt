package com.sharipov.memoriesandnotes.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.sharipov.memoriesandnotes.model.EventDisplaySettings

@Dao
interface EventDisplaySettingsDao : BaseDao<EventDisplaySettings> {

    @Query("SELECT * FROM events_display_settings")
    fun getAllEventsDisplaySettings(): LiveData<List<EventDisplaySettings>>

    @Query("SELECT * FROM events_display_settings WHERE event_id = :eventId")
    fun getEventDisplaySettings(eventId: Long): LiveData<EventDisplaySettings>

    @Query("SELECT * FROM events_display_settings WHERE event_id = :eventId")
    fun getEventDisplaySettingsNow(eventId: Long): EventDisplaySettings?

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertEventsDisplaySettingsConflictIgnore(eventsDisplaySettings: List<EventDisplaySettings>)

    @Query("DELETE FROM events_display_settings")
    fun deleteAllEventsDisplaySettings()

    @Query("DELETE FROM events_display_settings WHERE event_id = :eventId")
    fun deleteEventDisplaySettingsById(eventId: Long)
}