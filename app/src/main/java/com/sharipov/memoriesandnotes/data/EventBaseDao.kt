package com.sharipov.memoriesandnotes.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.sharipov.memoriesandnotes.model.EventBase

@Dao
interface EventBaseDao : BaseDao<EventBase> {

    @Query("SELECT * FROM events")
    fun getAllEvents(): LiveData<List<EventBase>>

    @Query("SELECT * FROM events WHERE id = :id")
    fun getEventBase(id: Long): LiveData<EventBase>

    @Query("SELECT * FROM events WHERE id = :id")
    fun getEventBaseNow(id: Long): EventBase


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertEventsConflictIgnore(events: List<EventBase>)

    @Query("DELETE FROM events")
    fun deleteAllEvents()

}
