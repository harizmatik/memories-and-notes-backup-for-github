package com.sharipov.memoriesandnotes.model

enum class Theme {
    DARK_THEME,
    DEFAULT_THEME,
    SYSTEM_THEME
}