package com.sharipov.memoriesandnotes.model

import com.sharipov.memoriesandnotes.model.EventDisplaySettings.CountingMethod
import java.time.Period
import java.time.temporal.ChronoUnit
import kotlin.math.floor

data class TimestampCounters(
    var years: Int? = null,
    var months: Int? = null,
    var weeks: Int? = null,
    var days: Int? = null
) {
    fun isSelected(method: CountingMethod) =
        when (method) {
            CountingMethod.YEARS -> years != null
            CountingMethod.MONTHS -> months != null
            CountingMethod.WEEKS -> weeks != null
            CountingMethod.DAYS -> days != null
        }

    companion object {

        fun countForEvent(
                event: EventBase,
                countingMethods: Set<CountingMethod>
        ): TimestampCounters {
            val result = TimestampCounters()

            var newStartDate = event.getStartDate()
            val finishDate = event.getFinishDate()
            val period = Period.between(newStartDate, finishDate)

            if (countingMethods.contains(CountingMethod.YEARS)) {
                result.years = period.years
                newStartDate = newStartDate.plusYears(result.years!!.toLong())
            }

            if (countingMethods.contains(CountingMethod.MONTHS)) {
                result.months = period.months + if (result.years == null) {
                    period.years * 12
                } else 0
                newStartDate = newStartDate.plusMonths(result.months!!.toLong())
            }

            var newDays = ChronoUnit.DAYS.between(newStartDate, finishDate).toInt()

            if (countingMethods.contains(CountingMethod.WEEKS)) {
                result.weeks = floor(newDays / 7.toDouble()).toInt()
                newDays -= result.weeks!! * 7
            }

            if (countingMethods.contains(CountingMethod.DAYS)) {
                result.days = newDays
            }

            return result
        }
    }
}