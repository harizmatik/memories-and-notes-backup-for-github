package com.sharipov.memoriesandnotes.model

import androidx.room.Embedded
import androidx.room.Relation
import java.util.*

data class EventWithDisplaySettings(
    @Embedded
    val base: EventBase,
    @Relation(parentColumn = "id", entityColumn = "event_id")
    val displaySettings: EventDisplaySettings?
) {
    fun toEvent(): Event {
        val safeDS = if (displaySettings != null) {
            if (displaySettings.orderId != null) {
                displaySettings
            } else {
                displaySettings.copy(orderId = Date().time)
            }
        } else {
            EventDisplaySettings(base.id, orderId = Date().time)
        }
        return Event(base, safeDS)
    }
}