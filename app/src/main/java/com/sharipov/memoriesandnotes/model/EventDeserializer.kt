package com.sharipov.memoriesandnotes.model

import com.google.gson.*
import java.lang.reflect.Type
import java.util.*

class EventDeserializer : JsonDeserializer<EventBase> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): EventBase {
        var result = EventBase(title = "", description = "")

        json?.let {
            val jsonObject = it.asJsonObject
            result = EventBase(
                id = jsonObject.get("id").asLong,
                title = jsonObject.get("title").asString,
                description = jsonObject.get("description").asString,
                date = Date(jsonObject.get("date").asLong),
                additionOrder = jsonObject.get("addition_date").asLong,
            )
        }

        return result
    }
}