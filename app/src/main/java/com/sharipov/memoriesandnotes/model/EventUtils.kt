package com.sharipov.memoriesandnotes.model

import android.appwidget.AppWidgetManager
import com.sharipov.memoriesandnotes.R
import java.time.LocalDate
import java.time.ZoneId
import java.time.temporal.ChronoUnit

fun EventBase.countDaysToToday() =
    ChronoUnit.DAYS.between(LocalDate.now(), getLocalDate()).toInt()

fun EventBase.getEventTimeStatusResId(): Int {
    val days = this.countDaysToToday()
    return if (days >= 0) {
        R.plurals.time_left
    } else {
        R.plurals.passed_time
    }
}

private fun EventBase.getLocalDate() = date.toInstant()
        .atZone(ZoneId.systemDefault())
        .toLocalDate()

fun EventBase.getStartDate(): LocalDate {
    val localDate = date.toInstant()
        .atZone(ZoneId.systemDefault())
        .toLocalDate()
    return if (localDate > LocalDate.now()) {
        LocalDate.now()
    } else {
        localDate
    }
}

fun EventBase.getFinishDate(): LocalDate {
    val localDate = date.toInstant()
        .atZone(ZoneId.systemDefault())
        .toLocalDate()
    return if (localDate < LocalDate.now()) {
        LocalDate.now()
    } else {
        localDate
    }
}
