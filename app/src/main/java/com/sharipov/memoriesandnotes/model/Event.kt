package com.sharipov.memoriesandnotes.model

data class Event(
    val base: EventBase,
    val displaySettings: EventDisplaySettings
)