package com.sharipov.memoriesandnotes.model

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import java.lang.reflect.Type

class EventSerializer : JsonSerializer<EventBase> {
    override fun serialize(src: EventBase?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        val result = JsonObject()

        src?.let {
            result.addProperty("id", it.id)
            result.addProperty("title", it.title)
            result.addProperty("description", it.description)
            result.addProperty("date", it.date.time)
            result.addProperty("addition_date", it.additionOrder)
        }

        return result
    }
}