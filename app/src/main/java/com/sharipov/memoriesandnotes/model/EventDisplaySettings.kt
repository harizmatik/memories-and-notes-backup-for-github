package com.sharipov.memoriesandnotes.model

import android.graphics.Color
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "events_display_settings")
data class EventDisplaySettings(
    @PrimaryKey @ColumnInfo(name = "event_id") val eventId: Long,
    @ColumnInfo(name = "counting_methods") val countingMethods: Set<CountingMethod> = DEFAULT_COUNTING_METHODS,
    @ColumnInfo(name = "order_id") var orderId: Long? = null,
    @ColumnInfo(name = "color") val color: Int = DEFAULT_COLOR
) {
    enum class CountingMethod(val mask: Int) {
        YEARS(0b1000),
        MONTHS(0b0100),
        WEEKS(0b0010),
        DAYS(0b0001)
    }

    enum class DefinedColors(val int: Int) {
        ORANGE(Color.parseColor("#f37f3d")),
        RED(Color.parseColor("#E53935")),
        PURPLE(Color.parseColor("#673AB7")),
        BLUE(Color.parseColor("#1E88E5")),
        GREEN(Color.parseColor("#43A047")),
        YELLOW(Color.parseColor("#FDD835")),
        CUSTOM(Color.parseColor("#7CB342"));

        companion object {
            fun convert(color: Int) = when (color) {
                Color.parseColor("#f37f3d") -> ORANGE
                Color.parseColor("#E53935") -> RED
                Color.parseColor("#673AB7") -> PURPLE
                Color.parseColor("#1E88E5") -> BLUE
                Color.parseColor("#43A047") -> GREEN
                Color.parseColor("#FDD835") -> YELLOW
                else -> CUSTOM
            }
        }
    }

    companion object {
        private val DEFAULT_COUNTING_METHODS = setOf(CountingMethod.DAYS)
        val DEFAULT_COLOR = DefinedColors.ORANGE.int
    }
}