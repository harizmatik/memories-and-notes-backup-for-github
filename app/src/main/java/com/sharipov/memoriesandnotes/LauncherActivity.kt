package com.sharipov.memoriesandnotes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sharipov.memoriesandnotes.data.AppPreferences
import com.sharipov.memoriesandnotes.login.Login
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LauncherActivity : AppCompatActivity() {
    @Inject
    lateinit var appPreferences: AppPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (appPreferences.usePassword) {
            startActivity(Intent(this, Login::class.java))
        }
        else{
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}