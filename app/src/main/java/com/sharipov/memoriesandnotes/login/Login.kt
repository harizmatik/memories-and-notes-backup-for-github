@file:Suppress("DEPRECATION")

package com.sharipov.memoriesandnotes.login

import android.Manifest
import android.annotation.SuppressLint
import android.app.KeyguardManager
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.preference.PreferenceManager
import com.sharipov.memoriesandnotes.MainActivity
import com.sharipov.memoriesandnotes.R
import com.sharipov.memoriesandnotes.data.AppPreferences
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_login.*
import javax.inject.Inject


@AndroidEntryPoint
@Suppress("DEPRECATION")
class Login : AppCompatActivity() {
    @Inject
    lateinit var appPreferences: AppPreferences
    private lateinit var viewModel: Login
    private var password: String = ""
    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_login)
    }

    override fun onBackPressed() {
        moveTaskToBack(true)
    }

    @SuppressLint("SetTextI18n")
    fun pushOneButton(view: View) {
        val passwordText = findViewById<View>(R.id.PasswordText) as TextView
        if (passwordText.text == resources.getString(R.string.login_text) || passwordText.text.isEmpty()) {
            passwordText.text = "●"
            password += '1'
        }
        else {
            if (passwordText.text.toString().length <= 16) {
                addDigit('1')
            }
            else{
                showToes("Максимальная длина пароля")
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun pushTwoButton(view: View) {
        val passwordText = findViewById<View>(R.id.PasswordText) as TextView
        if (passwordText.text == resources.getString(R.string.login_text) || passwordText.text.isEmpty()) {
            passwordText.text = "●"
            password += '2'
        }
        else {
            if (passwordText.text.toString().length <= 16) {
                addDigit('2')
            }
            else{
                showToes("Максимальная длина пароля")
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun pushThreeButton(view: View) {
        val passwordText = findViewById<View>(R.id.PasswordText) as TextView
        if (passwordText.text == resources.getString(R.string.login_text) || passwordText.text.isEmpty()) {
            passwordText.text = "●"
            password += '3'
        }
        else {
            if (passwordText.text.toString().length <= 16) {
                addDigit('3')
            }
            else{
                showToes("Максимальная длина пароля")
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun pushFourButton(view: View) {
        val passwordText = findViewById<View>(R.id.PasswordText) as TextView
        if (passwordText.text == resources.getString(R.string.login_text) || passwordText.text.isEmpty()) {
            passwordText.text = "●"
            password += '4'
        }
        else {
            if (passwordText.text.toString().length <= 16) {
                addDigit('4')
            }
            else{
                showToes("Максимальная длина пароля")
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun pushFiveButton(view: View) {
        val passwordText = findViewById<View>(R.id.PasswordText) as TextView
        if (passwordText.text == resources.getString(R.string.login_text) || passwordText.text.isEmpty()){
            passwordText.text = "●"
            password += '5'
        }
        else {
            if (passwordText.text.toString().length <= 16) {
                addDigit('5')
            }
            else{
                showToes("Максимальная длина пароля")
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun pushSixButton(view: View) {
        val passwordText = findViewById<View>(R.id.PasswordText) as TextView
        if (passwordText.text == resources.getString(R.string.login_text) || passwordText.text.isEmpty()){
            passwordText.text = "●"
            password += '6'
        }
        else {
            if (passwordText.text.toString().length <= 16) {
                addDigit('6')
            }
            else{
                showToes("Максимальная длина пароля")
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun pushSevenButton(view: View) {
        val passwordText = findViewById<View>(R.id.PasswordText) as TextView
        if (passwordText.text == resources.getString(R.string.login_text) || passwordText.text.isEmpty()){
            passwordText.text = "●"
            password += "7"
        }
        else {
            if (passwordText.text.toString().length <= 16) {
                addDigit('7')
            }
            else{
                showToes("Максимальная длина пароля")
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun pushEightButton(view: View) {
        val passwordText = findViewById<View>(R.id.PasswordText) as TextView
        if (passwordText.text == resources.getString(R.string.login_text) || passwordText.text.isEmpty()) {
            passwordText.text = "●"
            password += '8'
        }
        else {
            if (passwordText.text.toString().length <= 16) {
                addDigit('8')
            }
            else{
                showToes("Максимальная длина пароля")
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun pushNineButton(view: View) {
        val passwordText = findViewById<View>(R.id.PasswordText) as TextView
        if (passwordText.text == resources.getString(R.string.login_text) || passwordText.text.isEmpty()){
            passwordText.text = "●"
            password += '9'
        }
        else {
            if (passwordText.text.toString().length <= 16) {
                addDigit('9')
            }
            else{
                showToes("Максимальная длина пароля")
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun pushZeroButton(view: View) {
        val passwordText = findViewById<View>(R.id.PasswordText) as TextView
        if (passwordText.text == resources.getString(R.string.login_text) || passwordText.text.isEmpty()) {
            passwordText.text = "●"
            password += '0'
        }
        else {
            if (passwordText.text.toString().length <= 16) {
                addDigit('0')
            }
            else{
                showToes("Максимальная длина пароля")
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun pushRemoveButton(view: View) {
        if (password == appPreferences.password) {
            val intent = Intent(this, MainActivity::class.java)
            this.startActivity(intent)
        }
        else{
            password = ""
            val passwordText = findViewById<View>(R.id.PasswordText) as TextView
            passwordText.text = resources.getString(R.string.login_text)
            showToes("Пароль не правильно введен")
        }
    }

    @SuppressLint("SetTextI18n")
    fun addDigit(digit: Char){
        val passwordText = findViewById<View>(R.id.PasswordText) as TextView
        passwordText.text = passwordText.text.toString() + '●'
        password += digit
    }

    fun showToes(text: String){
        val duration = Toast.LENGTH_SHORT
        val toast = Toast.makeText(applicationContext, text, duration)
        toast.show()
    }

    @SuppressLint("SetTextI18n")
    fun pushClearButton(view: View) {
        val passwordText = findViewById<View>(R.id.PasswordText) as TextView
        if (!(passwordText.text == resources.getString(R.string.login_text) || passwordText.text.isEmpty())) {
            password = password.substring(0, (password.length - 1))
            if(password.isEmpty()) {
                passwordText.text = resources.getString(R.string.login_text)
            }
            else{
                passwordText.text = passwordText.text.toString().substring(0, (passwordText.text.length - 1))
            }
        }
    }

    companion object {
        private const val USE_TOUCH_SCREEN_BUTTON = "use_touch_screen_button"
    }
}