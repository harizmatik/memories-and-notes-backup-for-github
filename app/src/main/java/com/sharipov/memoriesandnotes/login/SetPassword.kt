package com.sharipov.memoriesandnotes.login

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.Preference
import androidx.preference.PreferenceManager
import com.sharipov.memoriesandnotes.R
import com.sharipov.memoriesandnotes.data.AppPreferences
import com.sharipov.memoriesandnotes.fragments.SettingsFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SetPassword : AppCompatActivity() {
    @Inject
    lateinit var appPreferences: AppPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_password)

    }

    override fun onBackPressed() {
        if (appPreferences.usePassword && appPreferences.password == "PasswordNotExist") {
            appPreferences.usePassword = false
        }
        super.onBackPressed()
    }

    fun setPassword(view: View) {
        val passField1 = findViewById<View>(R.id.input_password_1) as EditText
        val passField2 = findViewById<View>(R.id.input_password_2) as EditText
        if (passField1.text.toString() == passField2.text.toString()){
            val duration = Toast.LENGTH_SHORT
            val toastMessage = R.string.password_has_been_saved
            val toast = Toast.makeText(applicationContext, toastMessage, duration)
            toast.show()
            appPreferences.password = passField1.text.toString()
            super.onBackPressed()
        }
        else{
            val duration = Toast.LENGTH_SHORT
            val toast = Toast.makeText(applicationContext, R.string.passwords_not_compare, duration)
            toast.show()
        }
    }
}